// Program to check a given character is Vowel or Consonant

#include <stdio.h>
int main()
{
	char character;
	
	printf("Enter a letter : ");
	scanf("%c", &character);

    //Condition to check character is alphabet or not 
    if((character >='a' && character <= 'z') || (character >='A' && character <= 'Z'))
    {
    	//Check whether vowel or consonant
    	switch(character)
    	{
    		case 'a':
    		case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				printf("Vowel");
				break;
			default :
				printf("Consonant");
				
		}
    }
	else
		printf("%c Not an alphabet", character);
    		
	return 0;
}
