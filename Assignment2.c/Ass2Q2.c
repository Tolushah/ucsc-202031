// Program to check whether entered integer is odd or even

#include <stdio.h>
int main ()
{
	int num;
	
	printf("Enter an integer : ");
	scanf("%d", &num);
	
	//True if remainder is zero
	
	if(num%2 ==0)
		printf("%d is an Even integer", num);
		
	else
	    printf("%d is an Odd integer", num);
		
	return 0;		
}
