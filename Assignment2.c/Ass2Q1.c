 // Program to check whether entered number is either positive or negative or zero
 
#include <stdio.h>
int main()
{  
	int num;
	
	printf("Enter an integer : ");
	scanf("%d", &num);
	
	if (num == 0)   
    	printf(" Zero");

	else if(num>0) 
	    printf("%d is a Positive number",num);
	
	else  
	    printf("%d is a Negative number",num);
	
	return 0;
	
}
