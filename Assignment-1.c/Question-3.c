#include <stdio.h>
 // This will swap two numbers 
 
 int main()
 {
 	int num_1, num_2, x;
 	
 	printf(" Enter your first number");
 	scanf("%d", &num_1);
 	
 	printf("Enter your second number");
 	scanf("%d", &num_2);
 	
 	x     = num_1;
 	num_1 = num_2;
 	num_2 = x;
 	
 	printf("After swapping first number is %d second number is  %d", num_1, num_2);
 	
 	return 0;
 }
