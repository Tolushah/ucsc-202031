#include <stdio.h>
 // Read the radius and computes the area of the disk 
 
 int main(){
 	
 	float radius, area;
 	const double PI = 3.14;
 	
 	printf("Enter radius of the disk: ");
 	scanf("%f",&radius);
 	
 	area = radius * radius * PI;
 	
 	printf("Area of the disk is %f", area);
 	
 	return 0;
 	
 }
