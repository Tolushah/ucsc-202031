#include <stdio.h>
int main(){
	
	//Program to print multiplication tables from 1 to given number
	
	int number, i, j, multiplication;
	printf("Enter a number : ");
	scanf("%d",&number);
	
	for(i=1; i<=number;i++)
	{
		printf("\n Multiplication of %d\n",i);
		
		for(j=1; j<=10 ; j++)
		{
			//calculate multiplication
			multiplication = i*j;
			printf("\n %d * %d = %d",i,j,multiplication);
		}
		
		printf("\n-------------------------\n");
	}
	
	return 0;
}
