#include <stdio.h>
int main() {

 //Program to reverse a number
 
  int number, remainder, reverse=0;
  printf("Enter a number : ");
  scanf("%d",&number);
  
  while(number != 0)
  {
  	remainder = number%10;
  	reverse = reverse*10 + remainder;
  	number /= 10;
  }
  
  printf("Reversed number is %d",reverse);
  return 0;
}
