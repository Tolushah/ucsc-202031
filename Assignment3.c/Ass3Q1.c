#include <stdio.h>
int main()
{
	//Program to calculate sum of all given positive integers until we enter the 0 or a negative value
	int number=0, sum= 0;
	
	do{
		printf("Enter a positive integer : ");
		scanf("%d", &number);
		
		//check whether they are positive or 0 or negative
		if(number<=0)
			break;
		sum += number;
	}
	while(number>0);
	
		printf("Sum = %d",sum);
		
		return 0;
}
